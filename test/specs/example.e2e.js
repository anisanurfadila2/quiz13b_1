describe("Privy_ChangeOTP", () => {
  
  
  it("tests Privy_ChangeOTP", async () => {
    await browser.setWindowSize(1101, 800)
    await browser.url("https://privy.id/")
    await expect(browser).toHaveUrl("https://privy.id/")
    await browser.$("#nav > ul > li.fa-nav__item.d-none.d-sm-block > a > span").click()
    await browser.$("#__BVID__4").setValue("uat002")
    await browser.$("/html/body/div[1]/div/div[2]/form/div[2]").click()
    await browser.$("#__BVID__4").setValue("uat002")
    await browser.$("/html/body/div[1]/div").click()
    await browser.$("#tag-lg001").click()
    await browser.$("#__BVID__6").click()
    await browser.$("#__BVID__6").setValue("T")
    await browser.$("#__BVID__6").setValue("Akuntes2")
    await browser.$("aria/using your Privy account to continue").click()
    await browser.$("#tag-lg001").click()
    await browser.pause(10000)
    await browser.waitUntil(() => (
      browser.execute(() => true)
    ))
    //buka dashboard
    await expect(browser).toHaveUrl("https://app.privy.id/dashboard")
    await browser.pause(10000)
    ///gear
    await browser.$("#__layout div.sidebar-bottom > a").click()
    await browser.pause(15000)
    await browser.setTimeout ({ 'pageLoad':20000 });
   //security
    await browser.$("#__layout > div > div > div.layout-default__container > div.layout-default__submenu > div:nth-child(2) > div.submenu__body > div:nth-child(2)").click()
     await browser.pause(5000)
//klik dropdown
    await browser.setTimeout ({ 'pageLoad':20000 });
     await browser.$("#v-security-0__BV_toggle_").click()
     await browser.pause(5000)
     await browser.setTimeout ({ 'pageLoad':20000 });

    // //klik otp with email
     await browser.$("#v-security-0 > ul > li:nth-child(3) > a").click();
     await browser.pause(10000)

     //assert check value dropdown
   let select_drop = await $("#v-security-0__BV_toggle_")
   let span_drop = await select_drop.$$("span")
  expect(span_drop).toHaveText("Send OTP via Email")

  });
});

